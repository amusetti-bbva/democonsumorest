package com.borrame.consumorest.controladores;

import com.borrame.consumorest.modelo.ClientePuente;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("${api.puente.v1}")
public class ControladorPuente {

    @Value("${api.externa.v1}")
    private String urlExterna;

    @Autowired
    RestTemplate restTemplate;

    /*
    * GET /puente/v1/clientes/ -> 200 OK [{documento,nombre,edad,fechaNacimiento},...]
    * POST /puente/v1/clientes/ + {documento,nombre,edad,fechaNacimiento} -> 200 OK, Location
    */

    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody ClientePuente cliente)  {
        final var location = restTemplate.postForLocation(this.urlExterna, cliente);
        return ResponseEntity.ok().location(location).build();
    }

    @GetMapping
    public List<ClientePuente> obtenerClientes() {

        final var respuesta = restTemplate.getForEntity(urlExterna, String.class);
        if(respuesta.getStatusCode().isError())
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);

        final var strJson = respuesta.getBody();

        // Jackson - Leer/Escribir JSON.
        final ObjectMapper mapper = new ObjectMapper();
        final var lista = new ArrayList<ClientePuente>();
        try {
            final var raiz = mapper.readTree(strJson);
            final var it = raiz.get("_embedded").get("entityModelList").elements();
            while(it.hasNext()) {
                final var elemento = it.next();
                final var cliente = new ClientePuente();
                cliente.documento = elemento.get("documento").asText();
                cliente.nombre = elemento.get("nombre").asText();
                cliente.edad = elemento.get("edad").asText();
                cliente.fechaNacimiento = elemento.get("fechaNacimiento").asText();
                lista.add(cliente);
            }

        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }

        return lista;
    }


}
